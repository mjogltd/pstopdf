/**
 * Copyright 2016, MJOG Ltd., http://www.mjog.com/.
 *
 *
 * This software is released under the terms of an
 * GNU AGPL open source license. Please refer to:
 *  http://www.gnu.org/licenses/agpl.txt
 *
 * @author sabrinap
 * Usage:   This creates a docker image that exposes a Spring Rest Controller
 *          used to convert a postscript file to a PDF file.
 *          To use, send http post requests to '/ps2pdf/convert'
 *
 */
package com.mjog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
