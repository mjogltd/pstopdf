/**
 * Copyright 2016, MJOG Ltd., http://www.mjog.com/.
 *
 *
 * This software is released under the terms of an
 * GNU AGPL open source license. Please refer to:
 *  http://www.gnu.org/licenses/agpl.txt
 *
 * @author sabrinap
 * Usage:   This creates a docker image that exposes a Spring Rest Controller
 *          used to convert a postscript file to a PDF file.
 *          To use, send http post requests to '/ps2pdf/convert'
 *
 */
package com.mjog;

import org.ghost4j.Ghostscript;
import org.ghost4j.GhostscriptRevision;
import org.ghost4j.converter.ConverterException;
import org.ghost4j.converter.PDFConverter;
import org.ghost4j.document.DocumentException;
import org.ghost4j.document.PSDocument;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 *  This service takes postscript in a POST http request and returns the PDF conversion.
 */
@RestController
public class PsToPdfConverter {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PsToPdfConverter.class);

    @RequestMapping(path = "/ps2pdf/convert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)

    public ResponseEntity<byte[]> convert(HttpEntity<byte[]> requestEntity) throws DocumentException, IOException, ConverterException {

        logger.info("/ps2pdf/convert called");
        byte[] bytePDF = postscriptToPdf(requestEntity.getBody());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(new Long(bytePDF.length));
        responseHeaders.setContentType(MediaType.valueOf("application/pdf"));


        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]> (
                bytePDF,
                responseHeaders,
                HttpStatus.OK);

        return responseEntity;
    }

    /**
     * Implementation that uses byte arrays for I/O.
     *
     * @param postscript Postscript bytes
     * @return byte[] PDF bytes
     */
    public byte[] postscriptToPdf(byte[] postscript) throws IOException, ConverterException, DocumentException {
        ByteArrayOutputStream convertedPdf = new ByteArrayOutputStream();
        GhostscriptRevision rev = Ghostscript.getRevision();
        InputStream is = new ByteArrayInputStream(postscript);

        //load PostScript document
        PSDocument document = new PSDocument();
        document.load(is);

        //create converter
        PDFConverter converter = new PDFConverter();
        //convert
        converter.convert(document, convertedPdf);
        return convertedPdf.toByteArray();
    }

}
