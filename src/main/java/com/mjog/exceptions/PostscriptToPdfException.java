/**
 * Copyright 2016, MJOG Ltd., http://www.mjog.com/.
 *
 *
 * This software is released under the terms of an
 * GNU AGPL open source license. Please refer to:
 *  http://www.gnu.org/licenses/agpl.txt
 *
 * @author sabrinap
 * Usage:   This creates a docker image that exposes a Spring Rest Controller
 *          used to convert a postscript file to a PDF file.
 *          To use, send http post requests to '/ps2pdf/convert'
 *
 */
package com.mjog.exceptions;

/**
 *
 * @author sabrinap
 */
public class PostscriptToPdfException extends RuntimeException {

    public PostscriptToPdfException(String message) {
        super(message);
    }

    public PostscriptToPdfException(String message, Throwable cause) {
        super(message, cause);
    }

}
