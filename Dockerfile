#
# Copyright 2016, MJOG Ltd., http://www.mjog.com/.
#
#
# This software is released under the terms of an
# GNU AGPL open source license. Please refer to:
#  http://www.gnu.org/licenses/agpl.txt
#
# @author sabrinap
# Usage:   This creates a docker image that exposes a Spring Rest Controller
#          used to convert a postscript file to a PDF file.
#          To use, send http post requests to '/ps2pdf/convert'
#
#
FROM openjdk:8

MAINTAINER Sabrina Peoples

RUN apt-get update && apt-get -y install ghostscript && apt-get clean

VOLUME /tmp
ADD target/smart-print-pstopdf-1.0-SNAPSHOT.jar /app.jar
RUN sh -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.library.path=/usr/lib","-jar","/app.jar"]